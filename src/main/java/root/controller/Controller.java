/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import root.model.dao.PalabraDAO;

import root.model.entities.Palabra;

/**
 *
 * @author Paulo Rojas
 */
@WebServlet(name = "controller", urlPatterns = {"/controller"})
public class Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet controller</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            root.dlc.Palabra word = new root.dlc.Palabra();

            System.out.print("doPost:");
            String palabra1 = request.getParameter("palabra");
            //recuperar la palabra
            //servicio rest dicccionar local
            Client client = ClientBuilder.newClient();
            WebTarget myResource = client.target("http://DESKTOP-EQ7PPE4:8080/Serviciosrestapp-1.0-SNAPSHOT/api/diccionario/" + palabra1);

            word = myResource.request(MediaType.APPLICATION_JSON).header("api-key", "KEYSEC0001918").header("api-id", "111111").get(root.dlc.Palabra.class);

           //SERVICIO DICCIONARIO OXFORD 
           /* WebTarget myResource = client.target("https://diccionarioprl.herokuapp.com/api/diccionario/" + palabra1);
            json = myResource.request(MediaType.APPLICATION_JSON).header("api-key", "249576b51720ba08019db05e5ca0cd61").header("api-id", "94c42c4a").get(String.class);*/
            

            Palabra palabraBD = new Palabra();
            palabraBD.setPalabra(word.getPalabra());
            palabraBD.setSignificado(word.getDescripcion());

            PalabraDAO dao = new PalabraDAO();
            dao.create(palabraBD);

            //grabar en nuestra base de datos
            List<Palabra> words = dao.findPalabraEntities();
            System.out.println("Cantidad de palabras en BD" + words.size());

            request.setAttribute("words", words);
            request.getRequestDispatcher("listarpalabras.jsp").forward(request, response);

        } catch (Exception ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
