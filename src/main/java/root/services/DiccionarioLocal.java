/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package root.services;

import root.dlc.Palabra;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;



@Path("diccionario")

public class DiccionarioLocal {


    
   @GET
   @Path("/{idbuscar}")
   @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo(@HeaderParam("api-key") String apikey,@HeaderParam("api-id") String apiId, @PathParam("idbuscar") String idbuscar){
    //PRUEBA DICCIONARIO OXFORD     
    /*public String listarTodo(@HeaderParam("api-key") String apikey, @HeaderParam("api-id") String apiId, @PathParam("idbuscar") String idbuscar) {
        List<DiccionarioLocal> lista = dao.findDiccionarioLocalEntities();*/    
        System.out.println("REST diccionario:  apikey"+apikey);
        System.out.println("REST diccionario:  apiId"+apiId);
        System.out.println("REST diccionario :    palabra  "+idbuscar);
        
      /*  String idioma = "es";
        String word = idbuscar;
        String campo = "significado";
        String word_id = word.toLowerCase();
        String urlOxford = "https://od-api.oxforddictionaries.com/api/v2/entries/" + idioma + "/" + word_id; */
        
        
        Palabra word=new Palabra();
        word.setPalabra(idbuscar);
        word.setDescripcion("la palabra significa esto");
    
    
        return Response.ok(200).entity(word).build();
    }
  

}
