<%-- 
    Document   : lista
    Created on : 19-abr-2020, 18:12:53
    Author     : LiL WOLF
--%>

<%@page import="root.model.entities.Palabra"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<Palabra> words=(List<Palabra>)request.getAttribute("words");
    Iterator<Palabra> itPalabra=words.iterator();
%>    


<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista Palabras</title>
        <link rel="stylesheet" href="listarstyle.css">
    </head>
    <body>
        
        <form  name="form" action="controllerListado" method="POST">
        
        <h1>Registro Palabras</h1>
        
        
        <div id="table">    
        <table class="table table-dark">
    <thead>
        <tr>
         <th scope="col">Palabra</th>
         <th scope="col">Significado</th>
         
        </tr>
    </thead>
      <tbody>
          <%while(itPalabra.hasNext()){
              Palabra word=itPalabra.next();%>
          
        <tr>
         <td scope="row"><%=word.getPalabra()%></td>
         <td><%=word.getSignificado()%></td>
         
        
        </tr> 
     
       <%}%> 
    </tbody>
</table>
  
    
    <button type="submit" name="accion" value="volver" class="btn btn-primary" style="font-size: 23px; text-align: center; margin-top: 20px; margin-left: 42%; height: 50px; width: 100px" >Volver</button>
    
   
    
    
    
    
   
    
    
    
        </div>
    
        </form>
    </body>
</html>