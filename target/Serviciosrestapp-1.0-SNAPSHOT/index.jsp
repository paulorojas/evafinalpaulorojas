<%-- 
    Document   : index
    Created on : 19-abr-2020, 0:53:34
    Author     : LiL WOLF
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Diccionario</title>
        <link rel="stylesheet" href="style.css">
    </head>
    
    <body>
        
        <h1>Consultar Palabra</h1>
        <p>
        
        <form action="controller" method="POST">
        
        <p>
        <div id="input">
            <input  name="palabra" class="form-control" value="" required id="apellido">
        </div>    
            
            <button type="submit" value="consultar" class="btn-lg btn-secondary">Consultar</button>
        </form>    
        
        <footer>
                <div class="container">
                    <p style="color: white">Paulo Rojas - Taller de APP Empresariales</p>
                </div>
        </footer>    
        
    </body>
</html>
